package ru.nlmk.study.jse84;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.nlmk.study.jse84.service.ReportService;

@SpringBootTest
class Jse84ApplicationTests {

	@Autowired
	private ReportService reportService;

	@Test
	void contextLoads() throws Exception {
		reportService.exportExcell();
	}

}
