package ru.nlmk.study.jse84.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nlmk.study.jse84.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
}
