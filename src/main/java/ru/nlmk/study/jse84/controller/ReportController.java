package ru.nlmk.study.jse84.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nlmk.study.jse84.service.ReportService;

@RequestMapping("/report")
@RestController
public class ReportController {

    private final ReportService reportService;

    @Autowired
    public ReportController(ReportService reportService) {
        this.reportService = reportService;
    }

    @GetMapping(value = "/getpdf", produces = "application/pdf")
    public ResponseEntity<byte[]> getPdfReport() throws Exception {
        return ResponseEntity.ok(reportService.exportPDF());
    }

    @GetMapping(value = "/gethtml")
    public void getHtmlReport() throws Exception {
        reportService.exportHTML();
    }
}
