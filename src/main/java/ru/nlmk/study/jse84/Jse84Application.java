package ru.nlmk.study.jse84;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jse84Application {

	public static void main(String[] args) {
		SpringApplication.run(Jse84Application.class, args);
	}

}
