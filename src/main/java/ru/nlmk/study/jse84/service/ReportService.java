package ru.nlmk.study.jse84.service;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import ru.nlmk.study.jse84.model.Employee;
import ru.nlmk.study.jse84.repository.EmployeeRepository;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReportService {

    private int cellNum = 0;

    private final String reportPath = "C:/Users/Nuclear/Desktop/reports/";


    private final EmployeeRepository employeeRepository;

    @Autowired
    public ReportService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public byte[] exportPDF() throws Exception {
        List<Employee> employees = employeeRepository.findAll();

        JasperPrint jasperPrint = generateRowReport(employees);

        return JasperExportManager.exportReportToPdf(jasperPrint);
    }

    public void exportHTML() throws Exception {

        List<Employee> employees = employeeRepository.findAll();

        JasperPrint jasperPrint = generateRowReport(employees);

        JasperExportManager.exportReportToHtmlFile(jasperPrint, reportPath + "employee.html");
    }

    public void exportExcell() throws Exception {
        HSSFWorkbook workbook = new HSSFWorkbook();

        HSSFSheet sheet = workbook.createSheet("Employees");

        List<Employee> employees = employeeRepository.findAll();

        int rownum = 0;
        Row row = sheet.createRow(rownum);

        HSSFCellStyle style = createStyle(workbook);

        createCell(row, "ID", style);
        createCell(row, "First name", style);
        createCell(row, "Last name", style);
        createCell(row, "Position", style);
        createCell(row, "Salary", style);

        for (Employee employee : employees) {
            rownum++;

            row = sheet.createRow(rownum);

            Cell cell = row.createCell(0, CellType.STRING);
            cell.setCellValue(employee.getId());

            cell = row.createCell(1, CellType.STRING);
            cell.setCellValue(employee.getFirstName());

            cell = row.createCell(2, CellType.STRING);
            cell.setCellValue(employee.getLastName());

            cell = row.createCell(3, CellType.STRING);
            cell.setCellValue(employee.getPosition());

            cell = row.createCell(4, CellType.STRING);
            cell.setCellValue(employee.getSalary());
        }

        File file = new File(reportPath + "employees.xls");

        FileOutputStream outFile = new FileOutputStream(file);
        workbook.write(outFile);
    }

    private HSSFCellStyle createStyle(HSSFWorkbook workbook) {
        HSSFFont font = workbook.createFont();
        font.setBold(true);
        HSSFCellStyle style = workbook.createCellStyle();
        style.setFont(font);
        return style;
    }

    private void createCell(Row row, String name, HSSFCellStyle style) {
        Cell cell = row.createCell(cellNum++, CellType.STRING);
        cell.setCellValue(name);
        cell.setCellStyle(style);
    }


    private JasperPrint generateRowReport(Collection collection) throws Exception {
        File file = ResourceUtils.getFile("classpath:employees.jrxml");

        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());

        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(collection);

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("createdBy", "ru.nlmk.study");

        return JasperFillManager.fillReport(jasperReport, parameters, dataSource);
    }
}
